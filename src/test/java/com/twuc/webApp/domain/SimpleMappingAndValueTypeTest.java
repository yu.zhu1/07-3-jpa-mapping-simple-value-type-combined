package com.twuc.webApp.domain;

import com.twuc.webApp.CompanyProfile;
import com.twuc.webApp.CompanyProfileRepository;
import com.twuc.webApp.UserProfile;
import com.twuc.webApp.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest  extends JpaTestBase{
    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;




    @Test
    void should_get_id_city_and_street_when_save_company_profile() {
        ClosureValue<Long> objectClosureValue = new ClosureValue<>();

        flush(em ->{
            CompanyProfile companyProfile = companyProfileRepository.save(new CompanyProfile("Nantong", "QingNianEastStreet"));
            objectClosureValue.setValue(companyProfile.getId());
        });

        Optional<CompanyProfile> byId = companyProfileRepository.findById(objectClosureValue.getValue());
        if (byId.isPresent()) {
        CompanyProfile companyProfile = byId.get();
        assertEquals(Long.valueOf(1),companyProfile.getId());
        assertEquals("Nantong", companyProfile.getCity());
        assertEquals("QingNianEastStreet", companyProfile.getStreet());
        }
    }


    @Test
    void should_get_id_address_city_and_street_when_save_user_profile() {
        ClosureValue<Long> objectClosureValue = new ClosureValue<>();
        flush(em ->{
            UserProfile userProfile = userProfileRepository.save(new UserProfile("Nantong", "QingNianEastStreet"));
            objectClosureValue.setValue(userProfile.getId());
        });

        Optional<UserProfile> byId = userProfileRepository.findById(objectClosureValue.getValue());
        if (byId.isPresent()) {
        UserProfile userProfile = byId.get();
        assertEquals(Long.valueOf(1),userProfile.getId());
        assertEquals("Nantong", userProfile.getAddressCity());
        assertEquals("QingNianEastStreet", userProfile.getAddressStreet());
        }
    }

    @Test
    void should_throw_if_city_is_null_for_company_profile() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            flush(em ->{
                companyProfileRepository.save(new CompanyProfile(null, "QingNianEastStreet"));
            });
        });
    }

    @Test
    void should_throw_if_street_is_null_for_company_profile() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            flush(em ->{
                companyProfileRepository.save(new CompanyProfile("Nantong", null));
            });
        });
    }

    @Test
    void should_throw_if_city_address_is_null_for_user_profile() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            flush(em ->{
                userProfileRepository.save(new UserProfile(null, "QingNianEastStreet"));
            });
        });
    }

    @Test
    void should_throw_if_street_address_is_null_for_user_profile() {
        assertThrows(DataIntegrityViolationException.class, () -> {
            flush(em ->{
                userProfileRepository.save(new UserProfile("Nantong", null));
            });
        });
    }

}
